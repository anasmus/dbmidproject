﻿using DBMidProject.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject
{
    public partial class Main : Form
    {
        public Form activeForm = null;
        public Main()
        {
            InitializeComponent();
        }

        public void OpenChildForm(Form childForm)
        {
            activeForm?.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            MainPanel.Controls.Add(childForm);
            this.Tag = childForm;
            childForm.Tag = this;
            childForm.BringToFront();
            childForm.Show();
        }

        private void HomeBtn_Click(object sender, EventArgs e)
        {
            activeForm?.Close();
        }

        private void StudentBtn_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Students());
        }

        private void CLOBtn_Click(object sender, EventArgs e)
        {
            OpenChildForm(new CLOs());
        }

        private void RubricBtn_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Rubrics());
        }

        private void AssesmentBtn_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Assessment());
        }

        private void ResultsBtn_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Result());
        }

        private void AttendanceBtn_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Attendance());
        }

        private void ReportBtn_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Reports());
        }
    }
}
