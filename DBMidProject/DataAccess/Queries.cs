﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DBMidProject.BL;
using System.Xml.Linq;
using static System.ComponentModel.Design.ObjectSelectorEditor;
using DBMidProject.Forms;

namespace DBMidProject.DataAccess
{
    public static class Queries
    {
        public static SqlConnection con = Configuration.getInstance().getConnection();
        public static DataTable GetStudents()
        {
            SqlCommand cmd = new SqlCommand("SELECT Id, RegistrationNumber, FirstName, LastName, Contact, Email FROM Student JOIN Lookup ON Student.Status = Lookup.LookupId WHERE Lookup.Name = 'Active';", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void AddStudent(Student student)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO Student (FirstName, LastName, Contact, Email, RegistrationNumber, Status) VALUES (@first, @last, @contact, @email, @regNo, (SELECT LookupId FROM Lookup WHERE Name = 'Active'));", con);
            cmd.Parameters.AddWithValue("@first", student.FirstName);
            cmd.Parameters.AddWithValue("@last", student.LastName);
            cmd.Parameters.AddWithValue("@contact", student.Contact);
            cmd.Parameters.AddWithValue("@email", student.Email);
            cmd.Parameters.AddWithValue("@regNo", student.RegistrationNumber);
            cmd.ExecuteNonQuery();
        }

        public static void UpdateStudent(Student student)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Student SET FirstName=@first, LastName=@last, Contact=@contact, Email=@email, RegistrationNumber=@regNo WHERE Id=@id;", con);
            cmd.Parameters.AddWithValue("@id", student.Id);
            cmd.Parameters.AddWithValue("@first", student.FirstName);
            cmd.Parameters.AddWithValue("@last", student.LastName);
            cmd.Parameters.AddWithValue("@contact", student.Contact);
            cmd.Parameters.AddWithValue("@email", student.Email);
            cmd.Parameters.AddWithValue("@regNo", student.RegistrationNumber);
            cmd.ExecuteNonQuery();
        }

        public static void DeleteStudent(int id) 
        {
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Status = (SELECT LookupId FROM Lookup WHERE Name = 'InActive') WHERE Id = @id;", con);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.ExecuteNonQuery();
        }

        public static DataTable GetCLOs()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Clo;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void AddCLO(string name)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO Clo (Name, DateCreated, DateUpdated) Values (@name, @date, @date)", con);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@date", DateTime.Today.ToString());
            cmd.ExecuteNonQuery();
        }

        public static void DeleteCLO(int id)
        {
            SqlCommand cmd = new SqlCommand("DELETE FROM Clo WHERE ID=@id", con);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.ExecuteNonQuery();
        }

        public static DataTable GetRubric()
        {
            SqlCommand cmd = new SqlCommand("SELECT Rubric.Id, Rubric.Details, Clo.Name AS CLO, (SELECT Details FROM RubricLevel WHERE MeasurementLevel = 1 AND RubricId = Rubric.Id) AS LevelOne, (SELECT Details FROM RubricLevel WHERE MeasurementLevel = 2 AND RubricId = Rubric.Id) AS LevelTwo, (SELECT Details FROM RubricLevel WHERE MeasurementLevel = 3 AND RubricId = Rubric.Id) AS LevelThree, (SELECT Details FROM RubricLevel WHERE MeasurementLevel = 4 AND RubricId = Rubric.Id) AS LevelFour FROM Rubric JOIN Clo ON CloId = Clo.Id;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void AddRubric(int cloId, string details)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO Rubric (Id, CloId, Details) OUTPUT INSERTED.Id Values ((SELECT Max(Id) + 1 FROM Rubric), @id, @details)", con);
            cmd.Parameters.AddWithValue("@id", cloId);
            cmd.Parameters.AddWithValue("@details", details);
            
            int id = (int)cmd.ExecuteScalar();

            for (int i = 1; i <= 4; i++)
            {
                cmd = new SqlCommand("INSERT INTO RubricLevel (RubricId, Details, MeasurementLevel) Values (@id, '', @num)", con);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@num", i);
                cmd.ExecuteNonQuery();
            }
        }

        public static void UpdateRubricLevel(int id, List<string> levels)
        {
            SqlCommand cmd = new SqlCommand();
            int i = 1;
            foreach (var level in levels)
            {
                cmd = new SqlCommand("UPDATE RubricLevel SET Details = @details WHERE RubricId = @id AND MeasurementLevel = @level;", con);
                cmd.Parameters.AddWithValue("@details", level);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@level", i);
                i++;
                cmd.ExecuteNonQuery();
            }
        }

        public static DataTable GetAssesments()
        {
            SqlCommand cmd = new SqlCommand("SELECT Id, Title, TotalMarks, TotalWeightage FROM Assessment;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void AddAssment(Assesment assesment)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO Assessment (Title, DateCreated, TotalMarks, TotalWeightage) Values (@title, @date, @marks, @weightage);", con);
            cmd.Parameters.AddWithValue("@title", assesment.Title);
            cmd.Parameters.AddWithValue("@date", DateTime.Today.ToString());
            cmd.Parameters.AddWithValue("@marks", assesment.TotalMarks);
            cmd.Parameters.AddWithValue("@weightage", assesment.TotalWeightage);
            cmd.ExecuteNonQuery();
        }

        public static void UpdateAssesment(Assesment assesment)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Assessment SET Title=@title, TotalMarks=@marks, TotalWeightage=@weightage WHERE Id=@id", con);
            cmd.Parameters.AddWithValue("@id", assesment.Id);
            cmd.Parameters.AddWithValue("@title", assesment.Title);
            cmd.Parameters.AddWithValue("@marks", assesment.TotalMarks);
            cmd.Parameters.AddWithValue("@weightage", assesment.TotalWeightage);
            cmd.ExecuteNonQuery();
        }

        public static DataTable GetComponents(int id)
        {
            SqlCommand cmd = new SqlCommand("SELECT Id, Name, RubricId, TotalMarks FROM AssessmentComponent WHERE AssessmentId = @id;", con);
            cmd.Parameters.AddWithValue("@id", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void AddComponent(int assessmentId, string name, int rubricId, int totalMarks)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO AssessmentComponent (Name, RubricId, TotalMarks, DateCreated, DateUpdated, AssessmentId) VALUES (@name, @rubricId, @marks, @date, @date, @assessment);", con);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@rubricId", rubricId);
            cmd.Parameters.AddWithValue("@marks", totalMarks);
            cmd.Parameters.AddWithValue("@date", DateTime.Today.ToString());
            cmd.Parameters.AddWithValue("@assessment", assessmentId);
            cmd.ExecuteNonQuery();
        }

        public static DataTable GetResults()
        {
            SqlCommand cmd = new SqlCommand("SELECT R3.StudentId, R3.AssessmentComponentId, R3.RegistrationNumber, R3.Student, R3.AssessmentTitle, R3.AssessmentComponentName, RubricLevel.MeasurementLevel, ((RubricLevel.MeasurementLevel / 4.0) * ComponentMarks) AS ObtainedMarks FROM (SELECT * FROM (SELECT Student.Id AS StudentId, Student.RegistrationNumber, (Student.FirstName + ' ' + Student.LastName) AS Student FROM Student WHERE Student.Status = 5) R1 CROSS JOIN (SELECT AssessmentComponent.Id AS AssessmentComponentId, Assessment.Title AS AssessmentTitle, AssessmentComponent.Name AS AssessmentComponentName, AssessmentComponent.TotalMarks AS ComponentMarks FROM AssessmentComponent JOIN Assessment ON AssessmentId = Assessment.Id) R2) R3 LEFT JOIN StudentResult ON R3.AssessmentComponentId = StudentResult.AssessmentComponentId AND R3.StudentId = StudentResult.StudentId LEFT JOIN RubricLevel ON RubricMeasurementId = RubricLevel.Id ORDER BY R3.StudentId;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void AddResult(int studentId, int componentId, int level)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO StudentResult VALUES (@student,@component,(SELECT RubricLevel.Id FROM AssessmentComponent JOIN Rubric ON AssessmentComponent.RubricId = Rubric.Id JOIN RubricLevel ON Rubric.Id = RubricLevel.RubricId WHERE AssessmentComponent.Id = @component AND MeasurementLevel = @level), @date)", con);
            cmd.Parameters.AddWithValue("@student", studentId);
            cmd.Parameters.AddWithValue("@component", componentId);
            cmd.Parameters.AddWithValue("@level", level);
            cmd.Parameters.AddWithValue("@date", DateTime.Today.ToString());
            cmd.ExecuteNonQuery();
        }

        public static void UpdateResult(int studentId, int componentId, int level)
        {
            SqlCommand cmd = new SqlCommand("UPDATE StudentResult SET RubricMeasurementId = (SELECT RubricLevel.Id FROM AssessmentComponent JOIN Rubric ON AssessmentComponent.RubricId = Rubric.Id JOIN RubricLevel ON Rubric.Id = RubricLevel.RubricId WHERE AssessmentComponent.Id = @component AND MeasurementLevel = @level), EvaluationDate = @date WHERE StudentId = @student AND AssessmentComponentId = @component;", con);
            cmd.Parameters.AddWithValue("@student", studentId);
            cmd.Parameters.AddWithValue("@component", componentId);
            cmd.Parameters.AddWithValue("@level", level);
            cmd.Parameters.AddWithValue("@date", DateTime.Today.ToString());
            cmd.ExecuteNonQuery();
        }

        public static void CreateAttendance(string date)
        {
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM ClassAttendance WHERE AttendanceDate = @date;", con);
            cmd.Parameters.AddWithValue("@date", date);

            int id = 0;
            int count = (int)cmd.ExecuteScalar();
            if (count > 0)
            {
                cmd = new SqlCommand("SELECT Id FROM ClassAttendance WHERE AttendanceDate = @date;", con);
                cmd.Parameters.AddWithValue("@date", date);
                id = (int)cmd.ExecuteScalar();
            }
            else
            {
                cmd = new SqlCommand("INSERT INTO ClassAttendance (AttendanceDate) OUTPUT inserted.Id VALUES (@date);", con);
                cmd.Parameters.AddWithValue("@date", date);
                id = (int)cmd.ExecuteScalar();

                cmd = new SqlCommand("INSERT INTO StudentAttendance (AttendanceId, StudentId, AttendanceStatus) SELECT @id AS AttendanceId, Id AS StudentId, 1 AS AttendanceStatus FROM Student WHERE Student.Status = 5;", con);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
            }
        }

        public static DataTable GetAttendance(string date)
        {
            SqlCommand cmd = new SqlCommand("SELECT AttendanceId, StudentId, ClassAttendance.AttendanceDate, Student.RegistrationNumber, Lookup.Name AS AttendanceStatus FROM StudentAttendance JOIN ClassAttendance ON StudentAttendance.AttendanceId = ClassAttendance.Id JOIN Student ON Student.Id = StudentAttendance.StudentId JOIN Lookup ON AttendanceStatus = Lookup.LookupId WHERE ClassAttendance.AttendanceDate = @date;", con);
            cmd.Parameters.AddWithValue("@date", date);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static void UpdateAttendance(int attendanceId, int studentId, int attendanceStatus)
        {
            SqlCommand cmd = new SqlCommand("UPDATE StudentAttendance SET AttendanceStatus=@status WHERE AttendanceId=@aId AND StudentId=@sId", con);
            cmd.Parameters.AddWithValue("@status", attendanceStatus);
            cmd.Parameters.AddWithValue("@sId", studentId);
            cmd.Parameters.AddWithValue("@aId", attendanceId);
            cmd.ExecuteNonQuery();
        }

        public static DataTable GetAssessmentWiseResult()
        {
            SqlCommand cmd = new SqlCommand("SELECT R3.RegistrationNumber, R3.Student, R3.AssessmentTitle, R3.TotalMarks, COALESCE(SUM(((RubricLevel.MeasurementLevel / 4.0) * ComponentMarks)), 0) AS ObtainedMarks FROM (SELECT * FROM (SELECT Student.Id AS StudentId, Student.RegistrationNumber, (Student.FirstName + ' ' + Student.LastName) AS Student FROM Student WHERE Student.Status = 5) R1 CROSS JOIN (SELECT AssessmentComponent.Id AS AssessmentComponentId, Assessment.Title AS AssessmentTitle, AssessmentComponent.Name AS AssessmentComponentName, AssessmentComponent.TotalMarks AS ComponentMarks, Assessment.TotalMarks AS TotalMarks FROM AssessmentComponent JOIN Assessment ON AssessmentId = Assessment.Id) R2) R3 LEFT JOIN StudentResult ON R3.AssessmentComponentId = StudentResult.AssessmentComponentId AND R3.StudentId = StudentResult.StudentId LEFT JOIN RubricLevel ON RubricMeasurementId = RubricLevel.Id GROUP BY R3.RegistrationNumber, R3.Student, R3.AssessmentTitle, R3.TotalMarks ORDER BY R3.RegistrationNumber;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static DataTable GetStudentWiseResult()
        {
            SqlCommand cmd = new SqlCommand("SELECT R3.RegistrationNumber, R3.Student, SUM(R3.TotalMarks) AS TotalMarks, COALESCE(SUM(((RubricLevel.MeasurementLevel / 4.0) * ComponentMarks)), 0) AS ObtainedMarks FROM (SELECT * FROM (SELECT Student.Id AS StudentId, Student.RegistrationNumber, (Student.FirstName + ' ' + Student.LastName) AS Student FROM Student WHERE Student.Status = 5) R1 CROSS JOIN (SELECT AssessmentComponent.Id AS AssessmentComponentId, Assessment.Title AS AssessmentTitle, AssessmentComponent.Name AS AssessmentComponentName, AssessmentComponent.TotalMarks AS ComponentMarks, Assessment.TotalMarks AS TotalMarks FROM AssessmentComponent JOIN Assessment ON AssessmentId = Assessment.Id) R2) R3 LEFT JOIN StudentResult ON R3.AssessmentComponentId = StudentResult.AssessmentComponentId AND R3.StudentId = StudentResult.StudentId LEFT JOIN RubricLevel ON RubricMeasurementId = RubricLevel.Id GROUP BY R3.RegistrationNumber, R3.Student ORDER BY R3.RegistrationNumber;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static DataTable GetCloWiseResult()
        {
            SqlCommand cmd = new SqlCommand("SELECT Student.FirstName + ' ' + Student.LastName AS [Student Name], Student.RegistrationNumber AS [Registration Number], Clo.Name AS [CLO Name], SUM(AssessmentComponent.TotalMarks) AS [Total Marks], SUM((CAST(RubricLevel.MeasurementLevel AS float) / [max measurement level].maxLevel) * AssessmentComponent.TotalMarks) AS [Obtained Marks], (((SUM((CAST(RubricLevel.MeasurementLevel AS float) / [max measurement level].maxLevel) * AssessmentComponent.TotalMarks)) * 100) / (SUM(AssessmentComponent.TotalMarks))) AS [Percentage] FROM Student INNER JOIN StudentResult ON Student.Id = StudentResult.StudentId INNER JOIN RubricLevel ON StudentResult.RubricMeasurementId = RubricLevel.Id INNER JOIN AssessmentComponent ON AssessmentComponent.Id = StudentResult.AssessmentComponentId INNER JOIN Assessment ON Assessment.Id = AssessmentComponent.AssessmentId INNER JOIN Rubric ON Rubric.Id = RubricLevel.RubricId INNER JOIN Clo ON Clo.Id = Rubric.CloId INNER JOIN (SELECT RubricLevel.RubricId AS Rid, MAX(RubricLevel.MeasurementLevel) AS maxLevel FROM RubricLevel GROUP BY RubricLevel.RubricId) AS [max measurement level] ON [max measurement level].Rid = Rubric.Id GROUP BY Clo.Name, Student.FirstName, Student.LastName, Student.RegistrationNumber;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static DataTable GetAttendaceReport()
        {
            SqlCommand cmd = new SqlCommand("SELECT Student.FirstName + ' ' + Student.LastName AS [Student Name], Student.RegistrationNumber AS [Registration Number], Lookup.Name AS [Attendance Status], ClassAttendance.AttendanceDate FROM Student INNER JOIN StudentAttendance ON Student.Id = StudentAttendance.StudentId INNER JOIN ClassAttendance ON ClassAttendance.Id = StudentAttendance.AttendanceId INNER JOIN Lookup ON Lookup.LookupId = StudentAttendance.AttendanceStatus ORDER BY AttendanceDate;", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static DataTable GetInacticeStudents()
        {
            SqlCommand cmd = new SqlCommand("Select * from Student where Status=6", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static Dictionary<int, string> RunQuery(string query)
        {
            Dictionary<int, string> resultDictionary = new Dictionary<int, string>();
            SqlCommand command = new SqlCommand(query, con);
            SqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                int id = reader.GetInt32(0);
                string value = reader.GetString(1);
                resultDictionary.Add(id, value);
            }
            reader.Close();
            return resultDictionary;
        }
    }
}
