﻿using System;

namespace DBMidProject.BL
{
    public class Student
    {
        public int Id { get; set; }
        public string RegistrationNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }

        public Student(int id, string registrationNumber, string firstName, string lastName, string contact, string email)
        {
            Id = id;
            RegistrationNumber = registrationNumber;
            FirstName = firstName;
            LastName = lastName;
            Contact = contact;
            Email = email;
        }

        public Student(int id) 
        {
            Id = id;
        }
    }
}
