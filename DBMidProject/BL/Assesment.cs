﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBMidProject.BL
{
    public class Assesment
    {
        public int Id {  get; set; }
        public string Title { get; set; }
        public int TotalMarks { get; set; }
        public int TotalWeightage { get; set; }

        public Assesment(int id, string title, int totalMarks, int totalWeightage) 
        { 
            Id = id;
            Title = title;
            TotalMarks = totalMarks;
            TotalWeightage = totalWeightage;
        }

        public Assesment(int id) 
        {
            Id = id;
        }
    }
}
