﻿using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class AddCLO : Form
    {
        public Action LoadGrid;
        public AddCLO(Action loadGrid)
        {
            this.LoadGrid = loadGrid;
            InitializeComponent();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            string cloName = CLOTextbox.Text;
            Queries.AddCLO(cloName);
            LoadGrid();
            this.Close();
        }
    }
}
