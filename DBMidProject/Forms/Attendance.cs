﻿using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class Attendance : Form
    {
        public Dictionary<string, int> status = new Dictionary<string, int>() 
        {
            {"Present", 1 },
            {"Absent", 2},
            {"Leave", 3 },
            {"Late", 4}
        };

        string SelectedDate = string.Empty;
        public Attendance()
        {
            InitializeComponent();
            DatePicker.Text = DateTime.Today.ToString();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            SelectedDate = DatePicker.Value.ToString("yyyy-MM-dd");
            Queries.CreateAttendance(SelectedDate);
            LoadGrid();
        }

        public void LoadGrid()
        {
            DGView.DataSource = Queries.GetAttendance(SelectedDate);
        }

        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = DGView.SelectedRows[0];
            int attendanceId = (int)row.Cells[0].Value;
            int studentId = (int)row.Cells[1].Value;
            int attendanceStatus = status[StatusComboBox.Text];

            Queries.UpdateAttendance(attendanceId, studentId, attendanceStatus);
            LoadGrid();
        }

        private void DGView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void DGView_SelectionChanged(object sender, EventArgs e)
        {
        }

        private void DGView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            string statusValue = DGView.SelectedRows[0]?.Cells[4]?.Value.ToString();
            StatusComboBox.SelectedIndex = status[statusValue] - 1;
        }
    }
}
