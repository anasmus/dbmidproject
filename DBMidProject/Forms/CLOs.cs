﻿using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class CLOs : Form
    {
        public CLOs()
        {
            InitializeComponent();
            LoadGrid();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            Action loadGrid = LoadGrid;
            Form form = new AddCLO(loadGrid);
            form.ShowDialog();
        }

        public void LoadGrid()
        {
            DGView.DataSource = Queries.GetCLOs();
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            int id = (int)DGView.SelectedRows[0].Cells[0].Value;
            Queries.DeleteCLO(id);
            LoadGrid();
        }
    }
}
