﻿using DBMidProject.BL;
using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class AddStudent : Form
    {
        public Action LoadGrid;
        public Student student;
        public AddStudent(Action loadGrid, Student student = null)
        {
            this.LoadGrid = loadGrid;
            this.student = student;
            InitializeComponent();

            if (student != null )
            {
                FirstNameTextbox.Text = student.FirstName;
                LastNameTextbox.Text = student.LastName;
                RegistrationNumberTextbox.Text = student.RegistrationNumber;
                ContactTextbox.Text = student.Contact;
                EmailTextbox.Text = student.Email;
            }
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            if (student is null)
            {
                this.student = new Student(0);
            }
            student.FirstName = FirstNameTextbox.Text;
            student.LastName = LastNameTextbox.Text;
            student.RegistrationNumber = RegistrationNumberTextbox.Text;
            student.Contact = ContactTextbox.Text;
            student.Email = EmailTextbox.Text;

            if (student.Id == 0)
            {
                Queries.AddStudent(student);
            } 
            else
            {
                Queries.UpdateStudent(student);
            }
            LoadGrid();
            this.Close();
        }
    }
}
