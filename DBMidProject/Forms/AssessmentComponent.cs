﻿using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class AssessmentComponent : Form
    {
        public int AssessmentId;
        public AssessmentComponent(int id)
        {
            AssessmentId = id;
            InitializeComponent();
            LoadGrid();

            Dictionary<int, string> dict = Queries.RunQuery("SELECT Id, Details FROM Rubric");
            RubricComboBox.DataSource = new BindingSource(dict, null);
            RubricComboBox.DisplayMember = "Value";
            RubricComboBox.ValueMember = "Key";
        }

        public void LoadGrid()
        {
            DGView.DataSource = Queries.GetComponents(AssessmentId);
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            string name = NameTextbox.Text;
            int rubricId = (int)RubricComboBox.SelectedValue;
            int totalMarks = int.Parse(TotalMarksTextbox.Text);

            Queries.AddComponent(AssessmentId, name, rubricId, totalMarks);
            LoadGrid();

            NameTextbox.Text = string.Empty;
            RubricComboBox.SelectedIndex = 0;
            TotalMarksTextbox.Text = string.Empty;
        }
    }
}
