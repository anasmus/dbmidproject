﻿using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class Result : Form
    {
        public Result()
        {
            InitializeComponent();
            LoadGrid();
            LevelComboBox.SelectedIndex = 0;
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = DGView.SelectedRows[0];
            int studentId = (int)row.Cells[0].Value;
            int componentId = (int)row.Cells[1].Value;
            object existingLevel = row.Cells[6].Value;
            int level = int.Parse(LevelComboBox.Text);

            if (existingLevel is DBNull)
            {
                Queries.AddResult(studentId, componentId, level);
            }
            else
            {
                Queries.UpdateResult(studentId, componentId, level);
            }
            LoadGrid();
        }

        public void LoadGrid()
        {
            DGView.DataSource = Queries.GetResults();
        }
    }
}
