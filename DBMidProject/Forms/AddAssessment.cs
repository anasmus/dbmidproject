﻿using DBMidProject.BL;
using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class AddAssessment : Form
    {
        public Assesment assement;
        public Action LoadGrid;
        public AddAssessment(Action loadGrid, Assesment assesment = null)
        {
            this.LoadGrid = loadGrid;
            this.assement = assesment;
            InitializeComponent();

            if (assement != null )
            {
                AssessmentTitleTextbox.Text = assement.Title;
                AssessmentTotalMarksTextbox.Text = assesment.TotalMarks.ToString();
                AssessmentTotalWeightageTextbox.Text = assesment.TotalWeightage.ToString();
            }
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            if ( assement is null ) 
            {
                this.assement = new Assesment(0);
            }
            assement.Title = AssessmentTitleTextbox.Text;
            assement.TotalMarks = int.Parse(AssessmentTotalMarksTextbox.Text);
            assement.TotalWeightage = int.Parse(AssessmentTotalWeightageTextbox.Text);

            if (assement.Id == 0 )
            {
                Queries.AddAssment(assement);
            }
            else
            {
                Queries.UpdateAssesment(assement);
            }

            LoadGrid();
            this.Close();
        }
    }
}
