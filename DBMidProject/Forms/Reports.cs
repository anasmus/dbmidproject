﻿using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class Reports : Form
    {
        DataTable dt;
        string fileName;
        string title;
        public Reports()
        {
            InitializeComponent();
            ReportComboBox.SelectedIndex = 0;
            ReportComboBox_SelectedIndexChanged(null, null);
        }

        private void ReportComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ReportComboBox.SelectedIndex == 0) 
            {
                dt = Queries.GetAssessmentWiseResult();
                fileName = "AssessmentWiseResultReport";
                title = "Assessment Wise Result";
            }
            else if (ReportComboBox.SelectedIndex == 1)
            {
                dt = Queries.GetCloWiseResult();
                fileName = "CLOWiseResultReport";
                title = "CLO Wise Result";
            }
            else if (ReportComboBox.SelectedIndex == 2)
            {
                dt = Queries.GetStudentWiseResult();
                fileName = "StudentWiseResultReport";
                title = "Student Wise Result";
            }
            else if (ReportComboBox.SelectedIndex == 3)
            {
                dt = Queries.GetInacticeStudents();
                fileName = "InActiveStudentReport";
                title = "InActive Student Report";
            }
            else if (ReportComboBox.SelectedIndex == 4)
            {
                dt = Queries.GetAttendaceReport();
                fileName = "StudentAttendanceReport";
                title = "Student Attendance Report";
            }
            DGView.DataSource = dt;
        }

        private void GenBtn_Click(object sender, EventArgs e)
        {
            ReportGenerator.GeneratePDF(dt, $"../../{fileName}.pdf", title);
            MessageBox.Show($"Successfully Created the Report in ../../{fileName}.pdf");
        }
    }
}
