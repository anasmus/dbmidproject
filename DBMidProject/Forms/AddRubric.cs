﻿using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class AddRubric : Form
    {
        public Action LoadGrid;
        public AddRubric(Action loadGrid)
        {
            LoadGrid = loadGrid;
            InitializeComponent();
            Dictionary<int, string> dict = Queries.RunQuery("SELECT Id, Name FROM Clo");
            CLOComboBox.DataSource = new BindingSource(dict, null);
            CLOComboBox.DisplayMember = "Value";
            CLOComboBox.ValueMember = "Key";
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            int cloId = (int)CLOComboBox.SelectedValue;
            string details = RubricDetails.Text;
            Queries.AddRubric(cloId, details);
            LoadGrid();
            this.Close();
        }
    }
}
