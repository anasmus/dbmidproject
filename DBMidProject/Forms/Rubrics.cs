﻿using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class Rubrics : Form
    {
        public Rubrics()
        {
            InitializeComponent();
            LoadGrid();
        }


        private void AddRubricBtn_Click(object sender, EventArgs e)
        {
            Action loadGrid = LoadGrid;
            Form form = new AddRubric(loadGrid);
            form.ShowDialog();
        }

        private void AddLevelsBtn_Click(object sender, EventArgs e)
        {
            Action loadGrid = LoadGrid;
            DataGridViewRow row = DGView.SelectedRows[0];
            int id = (int)row.Cells[0].Value;
            string rl1 = (string)row.Cells[3].Value;
            string rl2 = (string)row.Cells[4].Value;
            string rl3 = (string)row.Cells[5].Value;
            string rl4 = (string)row.Cells[6].Value;
            Form form = new AddLevels(loadGrid, id, rl1, rl2, rl3, rl4);
            form.ShowDialog();
        }

        public void LoadGrid()
        {
            DGView.DataSource = Queries.GetRubric();
        }
    }
}
