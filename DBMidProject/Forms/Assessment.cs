﻿using DBMidProject.BL;
using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class Assessment : Form
    {
        public Assessment()
        {
            InitializeComponent();
            LoadGrid();
        }

        private void AddAssessmentBtn_Click(object sender, EventArgs e)
        {
            Action loadGrid = LoadGrid;
            Form form = new AddAssessment(loadGrid);
            form.ShowDialog();
        }

        public void LoadGrid()
        {
            DGView.DataSource = Queries.GetAssesments();
        }

        private void EditBtn_Click(object sender, EventArgs e)
        {
            Action loadGrid = LoadGrid;
            DataGridViewRow row = DGView.SelectedRows[0];
            int id = (int)row.Cells[0].Value;
            string title = (string)row.Cells[1].Value;
            int totalMarks = (int)row.Cells[2].Value;
            int totalWeightage = (int)row.Cells[3].Value;
            Assesment assesment = new Assesment(id, title, totalMarks, totalWeightage);

            Form form = new AddAssessment(loadGrid, assesment);
            form.ShowDialog();
        }

        private void AddComponentBtn_Click(object sender, EventArgs e)
        {
            int id = (int)DGView.SelectedRows[0].Cells[0].Value;
            Form form = new AssessmentComponent(id);
            form.ShowDialog();
        }
    }
}
