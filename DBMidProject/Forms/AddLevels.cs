﻿using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class AddLevels : Form
    {
        public int Id;
        public Action LoadGrid;
        public AddLevels(Action loadGrid, int id, string rl1,string rl2, string rl3, string rl4)
        {
            InitializeComponent();
            LevelOneDetailsTextbox.Text = rl1;
            LevelTwoDetailsTextbox.Text = rl2;
            LevelThreeDetailsTextbox.Text = rl3;
            LevelFourDetailsTextbox.Text = rl4;
            Id = id;
            LoadGrid = loadGrid;
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            List<string> list = new List<string>();
            list.Add(LevelOneDetailsTextbox.Text);
            list.Add(LevelTwoDetailsTextbox.Text);
            list.Add(LevelThreeDetailsTextbox.Text);
            list.Add(LevelFourDetailsTextbox.Text);

            Queries.UpdateRubricLevel(Id, list);

            LoadGrid();
            this.Close();
        }
    }
}
