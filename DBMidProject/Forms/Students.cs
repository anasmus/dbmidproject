﻿using DBMidProject.BL;
using DBMidProject.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject.Forms
{
    public partial class Students : Form
    {
        public Students()
        {
            InitializeComponent();
            LoadGrid();
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            Action loadGrid = LoadGrid;
            Form form = new AddStudent(loadGrid);
            form.ShowDialog();  
        }

        private void LoadGrid()
        {
            DGView.DataSource = Queries.GetStudents();
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            int id = (int)DGView.SelectedRows[0].Cells[0].Value;
            Queries.DeleteStudent(id);
            LoadGrid();
        }

        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = DGView.SelectedRows[0];
            int id = (int)row.Cells[0].Value;
            string regNo = (string)row.Cells[1].Value;
            string firstName = (string)row.Cells[2].Value;
            string lastName = (string)row.Cells[3].Value;
            string contact = (string)row.Cells[4].Value;
            string email = (string)row.Cells[5].Value;
            Student student = new Student(id, regNo, firstName, lastName, contact, email);
            Action loadGrid = LoadGrid;
            Form form = new AddStudent(loadGrid, student);
            form.ShowDialog();
        }
    }
}
