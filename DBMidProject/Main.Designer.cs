﻿namespace DBMidProject
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.AttendanceBtn = new Guna.UI2.WinForms.Guna2Button();
            this.ResultsBtn = new Guna.UI2.WinForms.Guna2Button();
            this.AssesmentBtn = new Guna.UI2.WinForms.Guna2Button();
            this.RubricBtn = new Guna.UI2.WinForms.Guna2Button();
            this.CLOBtn = new Guna.UI2.WinForms.Guna2Button();
            this.StudentBtn = new Guna.UI2.WinForms.Guna2Button();
            this.HomeBtn = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2ShadowPanel1 = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.guna2HtmlLabel1 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.MainPanel = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.guna2PictureBox2 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.ReportBtn = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Panel1.SuspendLayout();
            this.guna2Panel2.SuspendLayout();
            this.guna2ShadowPanel1.SuspendLayout();
            this.MainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.Controls.Add(this.ReportBtn);
            this.guna2Panel1.Controls.Add(this.AttendanceBtn);
            this.guna2Panel1.Controls.Add(this.ResultsBtn);
            this.guna2Panel1.Controls.Add(this.AssesmentBtn);
            this.guna2Panel1.Controls.Add(this.RubricBtn);
            this.guna2Panel1.Controls.Add(this.CLOBtn);
            this.guna2Panel1.Controls.Add(this.StudentBtn);
            this.guna2Panel1.Controls.Add(this.HomeBtn);
            this.guna2Panel1.Controls.Add(this.guna2Panel2);
            this.guna2Panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.guna2Panel1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(49)))), ((int)(((byte)(45)))));
            this.guna2Panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2Panel1.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel1.Margin = new System.Windows.Forms.Padding(0);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.Size = new System.Drawing.Size(250, 614);
            this.guna2Panel1.TabIndex = 0;
            // 
            // AttendanceBtn
            // 
            this.AttendanceBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(49)))), ((int)(((byte)(45)))));
            this.AttendanceBtn.BorderThickness = 1;
            this.AttendanceBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.AttendanceBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.AttendanceBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.AttendanceBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.AttendanceBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.AttendanceBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(197)))), ((int)(((byte)(178)))));
            this.AttendanceBtn.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.AttendanceBtn.ForeColor = System.Drawing.Color.White;
            this.AttendanceBtn.Location = new System.Drawing.Point(0, 420);
            this.AttendanceBtn.Name = "AttendanceBtn";
            this.AttendanceBtn.Size = new System.Drawing.Size(250, 50);
            this.AttendanceBtn.TabIndex = 7;
            this.AttendanceBtn.Text = "Attendance";
            this.AttendanceBtn.Click += new System.EventHandler(this.AttendanceBtn_Click);
            // 
            // ResultsBtn
            // 
            this.ResultsBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(49)))), ((int)(((byte)(45)))));
            this.ResultsBtn.BorderThickness = 1;
            this.ResultsBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ResultsBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ResultsBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ResultsBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ResultsBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.ResultsBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(197)))), ((int)(((byte)(178)))));
            this.ResultsBtn.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.ResultsBtn.ForeColor = System.Drawing.Color.White;
            this.ResultsBtn.Location = new System.Drawing.Point(0, 370);
            this.ResultsBtn.Name = "ResultsBtn";
            this.ResultsBtn.Size = new System.Drawing.Size(250, 50);
            this.ResultsBtn.TabIndex = 6;
            this.ResultsBtn.Text = "Results";
            this.ResultsBtn.Click += new System.EventHandler(this.ResultsBtn_Click);
            // 
            // AssesmentBtn
            // 
            this.AssesmentBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(49)))), ((int)(((byte)(45)))));
            this.AssesmentBtn.BorderThickness = 1;
            this.AssesmentBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.AssesmentBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.AssesmentBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.AssesmentBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.AssesmentBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.AssesmentBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(197)))), ((int)(((byte)(178)))));
            this.AssesmentBtn.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.AssesmentBtn.ForeColor = System.Drawing.Color.White;
            this.AssesmentBtn.Location = new System.Drawing.Point(0, 320);
            this.AssesmentBtn.Name = "AssesmentBtn";
            this.AssesmentBtn.Size = new System.Drawing.Size(250, 50);
            this.AssesmentBtn.TabIndex = 5;
            this.AssesmentBtn.Text = "Assesments";
            this.AssesmentBtn.Click += new System.EventHandler(this.AssesmentBtn_Click);
            // 
            // RubricBtn
            // 
            this.RubricBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(49)))), ((int)(((byte)(45)))));
            this.RubricBtn.BorderThickness = 1;
            this.RubricBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.RubricBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.RubricBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.RubricBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.RubricBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.RubricBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(197)))), ((int)(((byte)(178)))));
            this.RubricBtn.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.RubricBtn.ForeColor = System.Drawing.Color.White;
            this.RubricBtn.Location = new System.Drawing.Point(0, 270);
            this.RubricBtn.Name = "RubricBtn";
            this.RubricBtn.Size = new System.Drawing.Size(250, 50);
            this.RubricBtn.TabIndex = 4;
            this.RubricBtn.Text = "Rubrics";
            this.RubricBtn.Click += new System.EventHandler(this.RubricBtn_Click);
            // 
            // CLOBtn
            // 
            this.CLOBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(49)))), ((int)(((byte)(45)))));
            this.CLOBtn.BorderThickness = 1;
            this.CLOBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.CLOBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.CLOBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.CLOBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.CLOBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.CLOBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(197)))), ((int)(((byte)(178)))));
            this.CLOBtn.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.CLOBtn.ForeColor = System.Drawing.Color.White;
            this.CLOBtn.Location = new System.Drawing.Point(0, 220);
            this.CLOBtn.Name = "CLOBtn";
            this.CLOBtn.Size = new System.Drawing.Size(250, 50);
            this.CLOBtn.TabIndex = 3;
            this.CLOBtn.Text = "CLOs";
            this.CLOBtn.Click += new System.EventHandler(this.CLOBtn_Click);
            // 
            // StudentBtn
            // 
            this.StudentBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(49)))), ((int)(((byte)(45)))));
            this.StudentBtn.BorderThickness = 1;
            this.StudentBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.StudentBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.StudentBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.StudentBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.StudentBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.StudentBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(197)))), ((int)(((byte)(178)))));
            this.StudentBtn.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.StudentBtn.ForeColor = System.Drawing.Color.White;
            this.StudentBtn.Location = new System.Drawing.Point(0, 170);
            this.StudentBtn.Name = "StudentBtn";
            this.StudentBtn.Size = new System.Drawing.Size(250, 50);
            this.StudentBtn.TabIndex = 2;
            this.StudentBtn.Text = "Students";
            this.StudentBtn.Click += new System.EventHandler(this.StudentBtn_Click);
            // 
            // HomeBtn
            // 
            this.HomeBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(49)))), ((int)(((byte)(45)))));
            this.HomeBtn.BorderThickness = 1;
            this.HomeBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.HomeBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.HomeBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.HomeBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.HomeBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.HomeBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(197)))), ((int)(((byte)(178)))));
            this.HomeBtn.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.HomeBtn.ForeColor = System.Drawing.Color.White;
            this.HomeBtn.Location = new System.Drawing.Point(0, 120);
            this.HomeBtn.Name = "HomeBtn";
            this.HomeBtn.Size = new System.Drawing.Size(250, 50);
            this.HomeBtn.TabIndex = 1;
            this.HomeBtn.Text = "Home";
            this.HomeBtn.Click += new System.EventHandler(this.HomeBtn_Click);
            // 
            // guna2Panel2
            // 
            this.guna2Panel2.BackColor = System.Drawing.Color.Transparent;
            this.guna2Panel2.Controls.Add(this.guna2PictureBox2);
            this.guna2Panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2Panel2.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel2.Name = "guna2Panel2";
            this.guna2Panel2.Size = new System.Drawing.Size(250, 120);
            this.guna2Panel2.TabIndex = 0;
            // 
            // guna2ShadowPanel1
            // 
            this.guna2ShadowPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(49)))), ((int)(((byte)(45)))));
            this.guna2ShadowPanel1.Controls.Add(this.guna2HtmlLabel1);
            this.guna2ShadowPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2ShadowPanel1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(49)))), ((int)(((byte)(45)))));
            this.guna2ShadowPanel1.Location = new System.Drawing.Point(250, 0);
            this.guna2ShadowPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.guna2ShadowPanel1.Name = "guna2ShadowPanel1";
            this.guna2ShadowPanel1.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(182)))), ((int)(((byte)(195)))));
            this.guna2ShadowPanel1.Size = new System.Drawing.Size(833, 120);
            this.guna2ShadowPanel1.TabIndex = 1;
            // 
            // guna2HtmlLabel1
            // 
            this.guna2HtmlLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2HtmlLabel1.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel1.Font = new System.Drawing.Font("League Spartan", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2HtmlLabel1.ForeColor = System.Drawing.Color.White;
            this.guna2HtmlLabel1.Location = new System.Drawing.Point(314, 24);
            this.guna2HtmlLabel1.Name = "guna2HtmlLabel1";
            this.guna2HtmlLabel1.Size = new System.Drawing.Size(204, 73);
            this.guna2HtmlLabel1.TabIndex = 0;
            this.guna2HtmlLabel1.Text = "Eduko 2.0";
            this.guna2HtmlLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainPanel
            // 
            this.MainPanel.BackColor = System.Drawing.Color.Transparent;
            this.MainPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.MainPanel.BorderColor = System.Drawing.Color.Transparent;
            this.MainPanel.Controls.Add(this.guna2PictureBox1);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(126)))), ((int)(((byte)(127)))), ((int)(((byte)(131)))));
            this.MainPanel.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(182)))), ((int)(((byte)(195)))));
            this.MainPanel.Location = new System.Drawing.Point(250, 120);
            this.MainPanel.Margin = new System.Windows.Forms.Padding(0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(833, 494);
            this.MainPanel.TabIndex = 2;
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2PictureBox1.Image = global::DBMidProject.Properties.Resources.EdukoLogo;
            this.guna2PictureBox1.ImageRotate = 0F;
            this.guna2PictureBox1.Location = new System.Drawing.Point(158, 28);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.Size = new System.Drawing.Size(517, 439);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.guna2PictureBox1.TabIndex = 0;
            this.guna2PictureBox1.TabStop = false;
            // 
            // guna2PictureBox2
            // 
            this.guna2PictureBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.guna2PictureBox2.Image = global::DBMidProject.Properties.Resources.EdukoLogo;
            this.guna2PictureBox2.ImageRotate = 0F;
            this.guna2PictureBox2.Location = new System.Drawing.Point(46, 3);
            this.guna2PictureBox2.Name = "guna2PictureBox2";
            this.guna2PictureBox2.Size = new System.Drawing.Size(136, 112);
            this.guna2PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.guna2PictureBox2.TabIndex = 1;
            this.guna2PictureBox2.TabStop = false;
            // 
            // ReportBtn
            // 
            this.ReportBtn.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(49)))), ((int)(((byte)(45)))));
            this.ReportBtn.BorderThickness = 1;
            this.ReportBtn.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ReportBtn.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ReportBtn.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ReportBtn.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ReportBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.ReportBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(197)))), ((int)(((byte)(178)))));
            this.ReportBtn.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.ReportBtn.ForeColor = System.Drawing.Color.White;
            this.ReportBtn.Location = new System.Drawing.Point(0, 470);
            this.ReportBtn.Name = "ReportBtn";
            this.ReportBtn.Size = new System.Drawing.Size(250, 50);
            this.ReportBtn.TabIndex = 8;
            this.ReportBtn.Text = "Reports";
            this.ReportBtn.Click += new System.EventHandler(this.ReportBtn_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1083, 614);
            this.Controls.Add(this.MainPanel);
            this.Controls.Add(this.guna2ShadowPanel1);
            this.Controls.Add(this.guna2Panel1);
            this.Name = "Main";
            this.Text = "Main";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.guna2Panel1.ResumeLayout(false);
            this.guna2Panel2.ResumeLayout(false);
            this.guna2ShadowPanel1.ResumeLayout(false);
            this.guna2ShadowPanel1.PerformLayout();
            this.MainPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Guna.UI2.WinForms.Guna2ShadowPanel guna2ShadowPanel1;
        private Guna.UI2.WinForms.Guna2GradientPanel MainPanel;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel1;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox2;
        private Guna.UI2.WinForms.Guna2Button CLOBtn;
        private Guna.UI2.WinForms.Guna2Button StudentBtn;
        private Guna.UI2.WinForms.Guna2Button HomeBtn;
        private Guna.UI2.WinForms.Guna2Button RubricBtn;
        private Guna.UI2.WinForms.Guna2Button AssesmentBtn;
        private Guna.UI2.WinForms.Guna2Button ResultsBtn;
        private Guna.UI2.WinForms.Guna2Button AttendanceBtn;
        private Guna.UI2.WinForms.Guna2Button ReportBtn;
    }
}